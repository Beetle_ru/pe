#-------------------------------------------------
#
# Project created by QtCreator 2012-04-21T10:59:40
#
#-------------------------------------------------

QT       += sql

QT       -= gui

TARGET = PE_StateLine
TEMPLATE = lib
CONFIG += staticlib

DESTDIR = ../build/

SOURCES += pe_stateline.cpp

HEADERS += pe_stateline.h
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
