#-------------------------------------------------
#
# Project created by QtCreator 2012-04-25T11:19:52
#
#-------------------------------------------------

QT       += sql

QT       -= gui

TARGET = PE_DescriptionOfProfile
TEMPLATE = lib
CONFIG += staticlib

DESTDIR = ../build/

SOURCES += pe_descriptionofprofile.cpp

HEADERS += pe_descriptionofprofile.h
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
