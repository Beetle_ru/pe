#-------------------------------------------------
#
# Project created by QtCreator 2012-04-25T11:23:08
#
#-------------------------------------------------

QT       += sql

QT       -= gui

TARGET = PE_CreditAndReferences
TEMPLATE = lib
CONFIG += staticlib

DESTDIR = ../build/

SOURCES += pe_creditandreferences.cpp

HEADERS += pe_creditandreferences.h
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
