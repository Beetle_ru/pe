#ifndef PE_VISUALACCESSRIGHTS_H
#define PE_VISUALACCESSRIGHTS_H

#include <QtGui/QWidget>
#include "pe_formaccessrights.h"

class PE_VisualAccessRights : public QWidget
{
    Q_OBJECT
    
public:
    PE_VisualAccessRights(QWidget *parent = 0);
};

#endif
