#ifndef PE_FORMDESCRIPTIONOFPROFILE_H
#define PE_FORMDESCRIPTIONOFPROFILE_H

#include <QWidget>
//#include "pe_commons.h"
#include "locale_cahnge.h"

namespace Ui {
class PE_FormDescriptionOfProfile;
}

class PE_FormDescriptionOfProfile : public QWidget
{
    Q_OBJECT
    
public:
    explicit PE_FormDescriptionOfProfile(QWidget *parent = 0);
    ~PE_FormDescriptionOfProfile();
    
private:
    Ui::PE_FormDescriptionOfProfile *ui;

    void changeEvent(QEvent* event);
    QTranslator  *qt_translator;
};

#endif // PE_FORMDESCRIPTIONOFPROFILE_H
