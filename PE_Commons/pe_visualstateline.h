#ifndef PE_VISUALSTATELINE_H
#define PE_VISUALSTATELINE_H

#include <QtGui/QWidget>
#include "pe_formstateline.h"

class PE_VisualStateLine : public QWidget
{
    Q_OBJECT
    
public:
    PE_VisualStateLine(QWidget *parent = 0);
};

#endif
