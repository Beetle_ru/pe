#include "pe_visualstateline.h"
#include "pe_visualstatelineplugin.h"

#include <QtCore/QtPlugin>

PE_VisualStateLinePlugin::PE_VisualStateLinePlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void PE_VisualStateLinePlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;
    
    // Add extension registrations, etc. here
    
    m_initialized = true;
}

bool PE_VisualStateLinePlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *PE_VisualStateLinePlugin::createWidget(QWidget *parent)
{
    return new PE_VisualStateLine(parent);
}

QString PE_VisualStateLinePlugin::name() const
{
    return QLatin1String("PE_VisualStateLine");
}

QString PE_VisualStateLinePlugin::group() const
{
    return QLatin1String("");
}

QIcon PE_VisualStateLinePlugin::icon() const
{
    return QIcon();
}

QString PE_VisualStateLinePlugin::toolTip() const
{
    return QLatin1String("Графический виджет ");
}

QString PE_VisualStateLinePlugin::whatsThis() const
{
    return QLatin1String("");
}

bool PE_VisualStateLinePlugin::isContainer() const
{
    return false;
}

QString PE_VisualStateLinePlugin::domXml() const
{
    return QLatin1String("<widget class=\"PE_VisualStateLine\" name=\"pE_VisualStateLine\">\n</widget>\n");
}

QString PE_VisualStateLinePlugin::includeFile() const
{
    return QLatin1String("pe_visualstateline.h");
}

