HEADERS += pe_visualstateline.h \
    pe_formstateline.h \
    canvaspicker.h
SOURCES += pe_visualstateline.cpp \
    pe_formstateline.cpp \
    canvaspicker.cpp

FORMS += \
    pe_formstateline.ui



LIBS += -L$$PWD/../tools/qwt-6.0/lib/ -lqwt

INCLUDEPATH += $$PWD/../tools/qwt-6.0/src
DEPENDPATH += $$PWD/../tools/qwt-6.0/src
