#include "pe_visualcreditandreferences.h"
#include "pe_visualcreditandreferencesplugin.h"

#include <QtCore/QtPlugin>

PE_VisualCreditAndReferencesPlugin::PE_VisualCreditAndReferencesPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void PE_VisualCreditAndReferencesPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;
    
    // Add extension registrations, etc. here
    
    m_initialized = true;
}

bool PE_VisualCreditAndReferencesPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *PE_VisualCreditAndReferencesPlugin::createWidget(QWidget *parent)
{
    return new PE_VisualCreditAndReferences(parent);
}

QString PE_VisualCreditAndReferencesPlugin::name() const
{
    return QLatin1String("PE_VisualCreditAndReferences");
}

QString PE_VisualCreditAndReferencesPlugin::group() const
{
    return QLatin1String("");
}

QIcon PE_VisualCreditAndReferencesPlugin::icon() const
{
    return QIcon();
}

QString PE_VisualCreditAndReferencesPlugin::toolTip() const
{
    return QLatin1String("");
}

QString PE_VisualCreditAndReferencesPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool PE_VisualCreditAndReferencesPlugin::isContainer() const
{
    return false;
}

QString PE_VisualCreditAndReferencesPlugin::domXml() const
{
    return QLatin1String("<widget class=\"PE_VisualCreditAndReferences\" name=\"pE_VisualCreditAndReferences\">\n</widget>\n");
}

QString PE_VisualCreditAndReferencesPlugin::includeFile() const
{
    return QLatin1String("pe_visualcreditandreferences.h");
}

