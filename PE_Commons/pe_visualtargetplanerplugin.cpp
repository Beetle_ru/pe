#include "pe_visualtargetplaner.h"
#include "pe_visualtargetplanerplugin.h"

#include <QtCore/QtPlugin>

PE_VisualTargetPlanerPlugin::PE_VisualTargetPlanerPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void PE_VisualTargetPlanerPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;
    
    // Add extension registrations, etc. here
    
    m_initialized = true;
}

bool PE_VisualTargetPlanerPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *PE_VisualTargetPlanerPlugin::createWidget(QWidget *parent)
{
    return new PE_VisualTargetPlaner(parent);
}

QString PE_VisualTargetPlanerPlugin::name() const
{
    return QLatin1String("PE_VisualTargetPlaner");
}

QString PE_VisualTargetPlanerPlugin::group() const
{
    return QLatin1String("");
}

QIcon PE_VisualTargetPlanerPlugin::icon() const
{
    return QIcon();
}

QString PE_VisualTargetPlanerPlugin::toolTip() const
{
    return QLatin1String("");
}

QString PE_VisualTargetPlanerPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool PE_VisualTargetPlanerPlugin::isContainer() const
{
    return false;
}

QString PE_VisualTargetPlanerPlugin::domXml() const
{
    return QLatin1String("<widget class=\"PE_VisualTargetPlaner\" name=\"pE_VisualTargetPlaner\">\n</widget>\n");
}

QString PE_VisualTargetPlanerPlugin::includeFile() const
{
    return QLatin1String("pe_visualtargetplaner.h");
}

