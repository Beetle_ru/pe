#include "locale_cahnge.h"

int updateLocale(QWidget *mainWindow, QTranslator *translator) {
    static QLocale lastLocale;

    //ui->retranslateUi(this);
    //qDebug() << "PE_FormDescriptionOfProfile: Language Changed ";
    //qDebug() << "PE_FormDescriptionOfProfile: language " << mainWindow->locale().language();

    QString file_translate;
    if(lastLocale == mainWindow->locale().language()){
        return -1;
    }
    else {
        QTranslator *loadTester = new QTranslator;

        if(mainWindow->locale().language() == QLocale::Russian) {
            file_translate = QApplication::applicationDirPath () + "/locales/co/ru.qm";
            //qDebug() << "PE_FormDescriptionOfProfile: Language Russian ";
        }
        if(mainWindow->locale().language() == QLocale::English) {
            file_translate = QApplication::applicationDirPath () + "/locales/co/en.qm";
            //qDebug() << "PE_FormDescriptionOfProfile: Language English ";
        }
        //qDebug() << "PLUGIN Locales file loaded " << loadTester->load(file_translate);

        if(loadTester->load(file_translate)) {
            translator->load(file_translate);
            lastLocale = mainWindow->locale().language();
            //qDebug() << "PLUGIN Locales retorn 0 ";
            return 0;
            //mainWindow->retranslateUi(mainWindow);
        }
        else
            delete loadTester;
    }
    return -1;
}
