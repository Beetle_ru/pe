<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>PE_FormAccessRights</name>
    <message>
        <location filename="pe_formaccessrights.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pe_formaccessrights.ui" line="45"/>
        <source>Groups</source>
        <translation>Группы</translation>
    </message>
    <message>
        <location filename="pe_formaccessrights.ui" line="50"/>
        <source>Edit/Modify</source>
        <translation>Изменять</translation>
    </message>
    <message>
        <location filename="pe_formaccessrights.ui" line="55"/>
        <source>Remove</source>
        <translation>Удалять</translation>
    </message>
    <message>
        <location filename="pe_formaccessrights.ui" line="60"/>
        <source>View</source>
        <translation>Просматривать</translation>
    </message>
    <message>
        <location filename="pe_formaccessrights.ui" line="65"/>
        <source>Time Begin</source>
        <translation>Время Begin</translation>
    </message>
    <message>
        <location filename="pe_formaccessrights.ui" line="70"/>
        <source>Time End</source>
        <translation>Время End</translation>
    </message>
    <message>
        <location filename="pe_formaccessrights.ui" line="84"/>
        <source>table of access rights</source>
        <translation>Таблица прав доступа</translation>
    </message>
</context>
<context>
    <name>PE_FormCreditAndReferences</name>
    <message>
        <location filename="pe_formcreditandreferences.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pe_formcreditandreferences.ui" line="36"/>
        <source>Options</source>
        <translation>Опции</translation>
    </message>
    <message>
        <location filename="pe_formcreditandreferences.ui" line="50"/>
        <source>Description status</source>
        <translation>Состояние</translation>
    </message>
    <message>
        <location filename="pe_formcreditandreferences.ui" line="55"/>
        <source>Approval time</source>
        <translation>Время утверждения</translation>
    </message>
    <message>
        <location filename="pe_formcreditandreferences.ui" line="60"/>
        <source>Author email </source>
        <translation>EMail автора</translation>
    </message>
    <message>
        <location filename="pe_formcreditandreferences.ui" line="65"/>
        <source>Author name full</source>
        <translation>Полное имя автора</translation>
    </message>
    <message>
        <location filename="pe_formcreditandreferences.ui" line="70"/>
        <source>Author telephone </source>
        <translation>Телефон автора</translation>
    </message>
    <message>
        <location filename="pe_formcreditandreferences.ui" line="75"/>
        <source>Author organization</source>
        <translation>Организация автора</translation>
    </message>
    <message>
        <location filename="pe_formcreditandreferences.ui" line="80"/>
        <source>Controller name </source>
        <translation>Название контроллера</translation>
    </message>
    <message>
        <location filename="pe_formcreditandreferences.ui" line="85"/>
        <source>Category profile </source>
        <translation>Категория профиля</translation>
    </message>
    <message>
        <location filename="pe_formcreditandreferences.ui" line="90"/>
        <source>Tags</source>
        <translation>Тэги</translation>
    </message>
    <message>
        <location filename="pe_formcreditandreferences.ui" line="102"/>
        <source>Option value</source>
        <translation>Значение опции</translation>
    </message>
</context>
<context>
    <name>PE_FormDescriptionOfProfile</name>
    <message>
        <location filename="pe_formdescriptionofprofile.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pe_formdescriptionofprofile.ui" line="36"/>
        <source>List of indicators</source>
        <translation>Список индикаторов</translation>
    </message>
    <message>
        <location filename="pe_formdescriptionofprofile.ui" line="57"/>
        <source>Purpose</source>
        <translation>Цель использования</translation>
    </message>
    <message>
        <location filename="pe_formdescriptionofprofile.ui" line="87"/>
        <source>Profile parameters</source>
        <translation>Параметры профиля</translation>
    </message>
    <message>
        <location filename="pe_formdescriptionofprofile.ui" line="101"/>
        <source>name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="pe_formdescriptionofprofile.ui" line="106"/>
        <source>application</source>
        <translation>Описание</translation>
    </message>
    <message>
        <location filename="pe_formdescriptionofprofile.ui" line="111"/>
        <source>purpose</source>
        <translation>Цель использования</translation>
    </message>
    <message>
        <location filename="pe_formdescriptionofprofile.ui" line="116"/>
        <source>pros</source>
        <translation>Преимущества</translation>
    </message>
    <message>
        <location filename="pe_formdescriptionofprofile.ui" line="121"/>
        <source>cons</source>
        <translation>Недостатки</translation>
    </message>
    <message>
        <location filename="pe_formdescriptionofprofile.ui" line="133"/>
        <source>Parameter value</source>
        <translation>Значение параметра</translation>
    </message>
    <message>
        <location filename="pe_formdescriptionofprofile.ui" line="160"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Availability of data in the database&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Наличие в базе данных&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="pe_formdescriptionofprofile.ui" line="177"/>
        <source>Start:</source>
        <translation>Начало:</translation>
    </message>
    <message>
        <location filename="pe_formdescriptionofprofile.ui" line="203"/>
        <source>End:</source>
        <translation>Конец:</translation>
    </message>
</context>
<context>
    <name>PE_FormStateLine</name>
    <message>
        <location filename="pe_formstateline.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PE_FormToolBar</name>
    <message>
        <location filename="pe_formtoolbar.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pe_formtoolbar.ui" line="26"/>
        <source>Save</source>
        <translation type="unfinished">Сохранить</translation>
    </message>
    <message>
        <location filename="pe_formtoolbar.ui" line="29"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#000000;&quot;&gt;&lt;span style=&quot; font-family:&apos;Times New Roman&apos;; font-size:medium; color:#000000; background-color:#000000;&quot;&gt;Save&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#000000;&quot;&gt;&lt;span style=&quot; font-family:&apos;Times New Roman&apos;; font-size:medium; color:#000000; background-color:#000000;&quot;&gt;Сохранить&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="pe_formtoolbar.ui" line="59"/>
        <source>Test profile</source>
        <translation type="unfinished">Проверить профиль</translation>
    </message>
    <message>
        <location filename="pe_formtoolbar.ui" line="62"/>
        <source>Test</source>
        <translation type="unfinished">Тест</translation>
    </message>
    <message>
        <location filename="pe_formtoolbar.ui" line="88"/>
        <location filename="pe_formtoolbar.ui" line="91"/>
        <source>Undo</source>
        <translation type="unfinished">Отменить</translation>
    </message>
    <message>
        <location filename="pe_formtoolbar.ui" line="117"/>
        <location filename="pe_formtoolbar.ui" line="120"/>
        <source>Redo</source>
        <translation type="unfinished">Вернуть</translation>
    </message>
    <message>
        <location filename="pe_formtoolbar.ui" line="146"/>
        <source>Edit copy</source>
        <translation type="unfinished">Копировать</translation>
    </message>
    <message>
        <location filename="pe_formtoolbar.ui" line="149"/>
        <source>Copy</source>
        <translation type="unfinished">Копировать</translation>
    </message>
    <message>
        <location filename="pe_formtoolbar.ui" line="175"/>
        <source>Edit cut</source>
        <translation type="unfinished">Вырезать</translation>
    </message>
    <message>
        <location filename="pe_formtoolbar.ui" line="178"/>
        <source>Cut</source>
        <translation type="unfinished">Вырезать</translation>
    </message>
    <message>
        <location filename="pe_formtoolbar.ui" line="204"/>
        <source>Edit paste</source>
        <translation type="unfinished">Вставить</translation>
    </message>
    <message>
        <location filename="pe_formtoolbar.ui" line="207"/>
        <source>Paste</source>
        <translation type="unfinished">Вставить</translation>
    </message>
    <message>
        <location filename="pe_formtoolbar.ui" line="233"/>
        <location filename="pe_formtoolbar.ui" line="236"/>
        <source>Help</source>
        <translation type="unfinished">Справка</translation>
    </message>
    <message>
        <location filename="pe_formtoolbar.ui" line="262"/>
        <source>Search</source>
        <translation type="unfinished">Поиск</translation>
    </message>
    <message>
        <location filename="pe_formtoolbar.ui" line="265"/>
        <source>Serch</source>
        <translation type="unfinished">Поиск</translation>
    </message>
</context>
</TS>
