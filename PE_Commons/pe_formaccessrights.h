#ifndef PE_FORMACCESSRIGHTS_H
#define PE_FORMACCESSRIGHTS_H

#include <QWidget>
#include "locale_cahnge.h"

namespace Ui {
class PE_FormAccessRights;
}

class PE_FormAccessRights : public QWidget
{
    Q_OBJECT
    
public:
    explicit PE_FormAccessRights(QWidget *parent = 0);
    ~PE_FormAccessRights();
    
private:
    Ui::PE_FormAccessRights *ui;

    void changeEvent(QEvent* event);
    QTranslator  *qt_translator;
};

#endif // PE_FORMACCESSRIGHTS_H
