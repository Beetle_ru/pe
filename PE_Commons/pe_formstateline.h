#ifndef PE_FORMSTATELINE_H
#define PE_FORMSTATELINE_H

#include <QWidget>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include "locale_cahnge.h"

namespace Ui {
class PE_FormStateLine;
}

class PE_FormStateLine : public QWidget
{
    Q_OBJECT
    
public:
    explicit PE_FormStateLine(QWidget *parent = 0);
    ~PE_FormStateLine();
    
private:
    Ui::PE_FormStateLine *ui;

    void changeEvent(QEvent* event);
    QTranslator  *qt_translator;
};

#endif // PE_FORMSTATELINE_H
