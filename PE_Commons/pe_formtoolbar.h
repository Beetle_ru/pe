#ifndef PE_FORMTOOLBAR_H
#define PE_FORMTOOLBAR_H

#include <QWidget>
#include "locale_cahnge.h"

namespace Ui {
class PE_FormToolBar;
}

class PE_FormToolBar : public QWidget
{
    Q_OBJECT
    
public:
    explicit PE_FormToolBar(QWidget *parent = 0);
    ~PE_FormToolBar();
    
private:
    Ui::PE_FormToolBar *ui;

    void changeEvent(QEvent* event);
    QTranslator  *qt_translator;
};

#endif // PE_FORMTOOLBAR_H
