#include "pe_formcreditandreferences.h"
#include "ui_pe_formcreditandreferences.h"

PE_FormCreditAndReferences::PE_FormCreditAndReferences(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PE_FormCreditAndReferences)
{
    ui->setupUi(this);

    qt_translator = new QTranslator;
    qApp->installTranslator( qt_translator );

    QList<int> splitterOSizes;
    splitterOSizes.append(300);
    splitterOSizes.append(ui->splitterO->width()-splitterOSizes[0]);
    ui->splitterO->setSizes(splitterOSizes);
    //ui->splitterO->setSizes(QList<int>() << 300 << 700);
}

PE_FormCreditAndReferences::~PE_FormCreditAndReferences()
{
    delete ui;
}

void PE_FormCreditAndReferences::changeEvent(QEvent* event)
{
    if (event->type() == QEvent::LanguageChange) {
        /*if(updateLocale(this,qt_translator) == 0) {
            ui->retranslateUi(this);
        }*/
        updateLocale(this,qt_translator);
        ui->retranslateUi(this);
    }
}
