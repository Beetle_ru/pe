#include "pe_formaccessrights.h"
#include "ui_pe_formaccessrights.h"

PE_FormAccessRights::PE_FormAccessRights(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PE_FormAccessRights)
{
    ui->setupUi(this);

    qt_translator = new QTranslator;
    qApp->installTranslator( qt_translator );

    int width = 0;
    for(int i=1; i < ui->tableAccessRights->columnCount(); i++) {
        //ui->tableAccessRights->setColumnWidth(i,ui->tableAccessRights->width()/ui->tableAccessRights->columnCount());
        width += ui->tableAccessRights->columnWidth(i);
    }
    ui->tableAccessRights->setColumnWidth(0,width-5);
}

PE_FormAccessRights::~PE_FormAccessRights()
{
    delete ui;
}

void PE_FormAccessRights::changeEvent(QEvent* event)
{
    if (event->type() == QEvent::LanguageChange) {
        /*if(updateLocale(this,qt_translator) == 0) {
            ui->retranslateUi(this);
        }*/
        updateLocale(this,qt_translator);
        ui->retranslateUi(this);
    }
}
