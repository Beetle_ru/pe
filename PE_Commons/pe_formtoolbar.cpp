#include "pe_formtoolbar.h"
#include "ui_pe_formtoolbar.h"

PE_FormToolBar::PE_FormToolBar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PE_FormToolBar)
{
    ui->setupUi(this);

    qt_translator = new QTranslator;
    qApp->installTranslator( qt_translator );

}

PE_FormToolBar::~PE_FormToolBar()
{
    delete ui;
}

void PE_FormToolBar::changeEvent(QEvent* event)
{
    if (event->type() == QEvent::LanguageChange) {
        /*if(updateLocale(this,qt_translator) == 0) {
            ui->retranslateUi(this);
        }*/
        updateLocale(this,qt_translator);
        ui->retranslateUi(this);
    }
}
