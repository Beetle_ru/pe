#ifndef PE_VISUALDESCRIPTIONOFPROFILE_H
#define PE_VISUALDESCRIPTIONOFPROFILE_H

#include <QtGui/QWidget>
#include "pe_formdescriptionofprofile.h"

class PE_VisualDescriptionOfProfile : public QWidget
{
    Q_OBJECT
    
public:
    PE_VisualDescriptionOfProfile(QWidget *parent = 0);
};

#endif
