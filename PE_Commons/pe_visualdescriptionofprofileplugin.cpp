#include "pe_visualdescriptionofprofile.h"
#include "pe_visualdescriptionofprofileplugin.h"

#include <QtCore/QtPlugin>

PE_VisualDescriptionOfProfilePlugin::PE_VisualDescriptionOfProfilePlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void PE_VisualDescriptionOfProfilePlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;
    
    // Add extension registrations, etc. here
    
    m_initialized = true;
}

bool PE_VisualDescriptionOfProfilePlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *PE_VisualDescriptionOfProfilePlugin::createWidget(QWidget *parent)
{
    return new PE_VisualDescriptionOfProfile(parent);
}

QString PE_VisualDescriptionOfProfilePlugin::name() const
{
    return QLatin1String("PE_VisualDescriptionOfProfile");
}

QString PE_VisualDescriptionOfProfilePlugin::group() const
{
    return QLatin1String("");
}

QIcon PE_VisualDescriptionOfProfilePlugin::icon() const
{
    return QIcon();
}

QString PE_VisualDescriptionOfProfilePlugin::toolTip() const
{
    return QLatin1String("");
}

QString PE_VisualDescriptionOfProfilePlugin::whatsThis() const
{
    return QLatin1String("");
}

bool PE_VisualDescriptionOfProfilePlugin::isContainer() const
{
    return false;
}

QString PE_VisualDescriptionOfProfilePlugin::domXml() const
{
    return QLatin1String("<widget class=\"PE_VisualDescriptionOfProfile\" name=\"pE_VisualDescriptionOfProfile\">\n</widget>\n");
}

QString PE_VisualDescriptionOfProfilePlugin::includeFile() const
{
    return QLatin1String("pe_visualdescriptionofprofile.h");
}

