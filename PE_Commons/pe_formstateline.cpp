#include "pe_formstateline.h"
#include "ui_pe_formstateline.h"
#include <qwt_symbol.h>
#include "canvaspicker.h"

PE_FormStateLine::PE_FormStateLine(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PE_FormStateLine)
{
    ui->setupUi(this);

    qt_translator = new QTranslator;
    qApp->installTranslator( qt_translator );

    QwtPlot *myPlot = new QwtPlot(this);
    // Создается первая линия
    QwtPlotCurve *curve1 = new QwtPlotCurve("H ot T");
    curve1->setRenderHint(QwtPlotItem::RenderAntialiased); // Устанавливается параметр сглаживания для этой кривой

    // Создается вторая линия
    QwtPlotCurve *curve2 = new QwtPlotCurve("Curve 2");

    // В области рисования обозначаются оси координат
    myPlot->setAxisTitle(QwtPlot::xBottom, tr("Days")); // Подпись горизонтальной оси
    myPlot->setAxisScale(QwtPlot::xBottom, 0, 1000); // Масштаб горизонтальной оси
    myPlot->setAxisTitle(QwtPlot::yLeft, tr("Units")); // Подпись вертикальной оси

    // Устанавливается цвет для второй линии
    curve2->setPen(QPen(Qt::blue));

    // Задается геометрия линий, согласно заранее подготовленным данным

    double x[40];
    double y[sizeof( x ) / sizeof( x[0] )];
    double d=0;
    for ( uint i = 0; i < sizeof( x ) / sizeof( x[0] ); i++ )
       {
           //double v = 5.0 + i * 10.0;
          x[i] = i*25;
          y[i] = sin(i);
          //y[i] = sin(d)+sin(d*sin(i*0.01));
          //d+=0.01;
       }
    curve1->setSamples(x,y,sizeof( x ) / sizeof( x[0] ));
    QPen pen;
    QColor c;

    c.setBlue(200);
    pen.setColor(c);
    pen.setWidth(3);
    curve1->setPen(pen);

    curve1->setSymbol( new QwtSymbol( QwtSymbol::Ellipse,Qt::gray, c, QSize( 8, 8 ) ) ); // добавляем кружочки

    // Линии размещаются на области рисования
    curve1->attach(myPlot);
    curve2->attach(myPlot);

    // И наконец, обновляется область рисования
    //myPlot->resize(800, 600); // Устанавливается размер области рисования
    myPlot->setGeometry(this->geometry());
    myPlot->replot(); // Область рисования перерисовывается
    ( void ) new CanvasPicker( myPlot );
}

PE_FormStateLine::~PE_FormStateLine()
{
    delete ui;
}

void PE_FormStateLine::changeEvent(QEvent* event)
{
    if (event->type() == QEvent::LanguageChange) {
        /*if(updateLocale(this,qt_translator) == 0) {
            ui->retranslateUi(this);
        }*/
        updateLocale(this,qt_translator);
        ui->retranslateUi(this);
    }
}
