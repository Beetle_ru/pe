#ifndef PE_VISUALCREDITANDREFERENCESPLUGIN_H
#define PE_VISUALCREDITANDREFERENCESPLUGIN_H

#include <QDesignerCustomWidgetInterface>

class PE_VisualCreditAndReferencesPlugin : public QObject, public QDesignerCustomWidgetInterface
{
    Q_OBJECT
    Q_INTERFACES(QDesignerCustomWidgetInterface)
    
public:
    PE_VisualCreditAndReferencesPlugin(QObject *parent = 0);
    
    bool isContainer() const;
    bool isInitialized() const;
    QIcon icon() const;
    QString domXml() const;
    QString group() const;
    QString includeFile() const;
    QString name() const;
    QString toolTip() const;
    QString whatsThis() const;
    QWidget *createWidget(QWidget *parent);
    void initialize(QDesignerFormEditorInterface *core);
    
private:
    bool m_initialized;
};

#endif
