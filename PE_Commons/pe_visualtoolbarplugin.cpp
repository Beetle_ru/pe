#include "pe_visualtoolbar.h"
#include "pe_visualtoolbarplugin.h"

#include <QtCore/QtPlugin>

PE_VisualToolBarPlugin::PE_VisualToolBarPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void PE_VisualToolBarPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;
    
    // Add extension registrations, etc. here
    
    m_initialized = true;
}

bool PE_VisualToolBarPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *PE_VisualToolBarPlugin::createWidget(QWidget *parent)
{
    return new PE_VisualToolBar(parent);
}

QString PE_VisualToolBarPlugin::name() const
{
    return QLatin1String("PE_VisualToolBar");
}

QString PE_VisualToolBarPlugin::group() const
{
    return QLatin1String("");
}

QIcon PE_VisualToolBarPlugin::icon() const
{
    return QIcon();
}

QString PE_VisualToolBarPlugin::toolTip() const
{
    return QLatin1String("");
}

QString PE_VisualToolBarPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool PE_VisualToolBarPlugin::isContainer() const
{
    return false;
}

QString PE_VisualToolBarPlugin::domXml() const
{
    return QLatin1String("<widget class=\"PE_VisualToolBar\" name=\"pE_VisualToolBar\">\n</widget>\n");
}

QString PE_VisualToolBarPlugin::includeFile() const
{
    return QLatin1String("pe_visualtoolbar.h");
}

