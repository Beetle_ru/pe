#ifndef PE_VISUALTOOLBAR_H
#define PE_VISUALTOOLBAR_H

#include <QtGui/QWidget>

class PE_VisualToolBar : public QWidget
{
    Q_OBJECT
    
public:
    PE_VisualToolBar(QWidget *parent = 0);
};

#endif
