#include "pe_formdescriptionofprofile.h"
#include "ui_pe_formdescriptionofprofile.h"


PE_FormDescriptionOfProfile::PE_FormDescriptionOfProfile(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PE_FormDescriptionOfProfile)
{
    ui->setupUi(this);

    qt_translator = new QTranslator;
    qApp->installTranslator( qt_translator );

    QList<int> splitterPPAPVSizes;
    splitterPPAPVSizes.append(300);
    splitterPPAPVSizes.append(ui->splitterPPAPV->width()-splitterPPAPVSizes[0]);
    ui->splitterPPAPV->setSizes(splitterPPAPVSizes);
}

PE_FormDescriptionOfProfile::~PE_FormDescriptionOfProfile()
{
    delete ui;
}

void PE_FormDescriptionOfProfile::changeEvent(QEvent* event)
{
    if (event->type() == QEvent::LanguageChange) {
        /*if(updateLocale(this,qt_translator) == 0) {
            ui->retranslateUi(this);
        }*/
        updateLocale(this,qt_translator);
        ui->retranslateUi(this);
    }
}
