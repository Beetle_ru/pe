CONFIG      += designer plugin debug_and_release
TARGET      = $$qtLibraryTarget(pe_commonsplugin)
TEMPLATE    = lib

HEADERS     = pe_visualstatelineplugin.h pe_visualtoolbarplugin.h pe_visualdescriptionofprofileplugin.h pe_visualtargetplanerplugin.h pe_visualaccessrightsplugin.h pe_visualcreditandreferencesplugin.h pe_commons.h \
    locale_cahnge.h
SOURCES     = pe_visualstatelineplugin.cpp pe_visualtoolbarplugin.cpp pe_visualdescriptionofprofileplugin.cpp pe_visualtargetplanerplugin.cpp pe_visualaccessrightsplugin.cpp pe_visualcreditandreferencesplugin.cpp pe_commons.cpp \
    locale_change.cpp
RESOURCES   = icons.qrc
LIBS        += -L. 

target.path = $$[QT_INSTALL_PLUGINS]/designer
INSTALLS    += target

DESTDIR = ../build/

include(pe_visualcreditandreferences.pri)
include(pe_visualtargetplaner.pri)
include(pe_visualaccessrights.pri)
include(pe_visualtoolbar.pri)
include(pe_visualstateline.pri)
include(pe_visualdescriptionofprofile.pri)

TRANSLATIONS += ru.ts
TRANSLATIONS += en.ts
