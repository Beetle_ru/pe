#ifndef PE_COMMONS_H
#define PE_COMMONS_H

#include <QtDesigner/QtDesigner>
#include <QtCore/qplugin.h>
#include <QTranslator>
#include <QtDebug>

#define VStateLine 0                  //PE_VisualStateLinePlugin
#define VToolBar 1                   //PE_VisualToolBarPlugin
#define VDescriptionOfProfile 2      //PE_VisualDescriptionOfProfilePlugin
#define VTargetPlaner 3               //PE_VisualTargetPlanerPlugin
#define VAccessRights 4               //PE_VisualAccessRightsPlugin
#define VCreditAndReferences 5        //PE_VisualCreditAndReferencesPlugin

class Q_DECL_EXPORT PE_Commons : public QObject, public QDesignerCustomWidgetCollectionInterface
{
    Q_OBJECT
    Q_INTERFACES(QDesignerCustomWidgetCollectionInterface)
    
public:
    explicit PE_Commons(QObject *parent = 0);
    
    virtual QList<QDesignerCustomWidgetInterface*> customWidgets() const;
    
private:
    QList<QDesignerCustomWidgetInterface*> m_widgets;
};

#endif
