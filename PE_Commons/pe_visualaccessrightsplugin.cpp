#include "pe_visualaccessrights.h"
#include "pe_visualaccessrightsplugin.h"

#include <QtCore/QtPlugin>

PE_VisualAccessRightsPlugin::PE_VisualAccessRightsPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void PE_VisualAccessRightsPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;
    
    // Add extension registrations, etc. here
    
    m_initialized = true;
}

bool PE_VisualAccessRightsPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *PE_VisualAccessRightsPlugin::createWidget(QWidget *parent)
{
    return new PE_VisualAccessRights(parent);
}

QString PE_VisualAccessRightsPlugin::name() const
{
    return QLatin1String("PE_VisualAccessRights");
}

QString PE_VisualAccessRightsPlugin::group() const
{
    return QLatin1String("");
}

QIcon PE_VisualAccessRightsPlugin::icon() const
{
    return QIcon();
}

QString PE_VisualAccessRightsPlugin::toolTip() const
{
    return QLatin1String("");
}

QString PE_VisualAccessRightsPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool PE_VisualAccessRightsPlugin::isContainer() const
{
    return false;
}

QString PE_VisualAccessRightsPlugin::domXml() const
{
    return QLatin1String("<widget class=\"PE_VisualAccessRights\" name=\"pE_VisualAccessRights\">\n</widget>\n");
}

QString PE_VisualAccessRightsPlugin::includeFile() const
{
    return QLatin1String("pe_visualaccessrights.h");
}

