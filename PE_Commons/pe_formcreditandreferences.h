#ifndef PE_FORMCREDITANDREFERENCES_H
#define PE_FORMCREDITANDREFERENCES_H

#include <QWidget>
#include "locale_cahnge.h"

namespace Ui {
class PE_FormCreditAndReferences;
}

class PE_FormCreditAndReferences : public QWidget
{
    Q_OBJECT
    
public:
    explicit PE_FormCreditAndReferences(QWidget *parent = 0);
    ~PE_FormCreditAndReferences();
    
private:
    Ui::PE_FormCreditAndReferences *ui;

    void changeEvent(QEvent* event);
    QTranslator  *qt_translator;
};

#endif // PE_FORMCREDITANDREFERENCES_H
