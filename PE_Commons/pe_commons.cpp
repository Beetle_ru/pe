#include "pe_visualstatelineplugin.h"
#include "pe_visualtoolbarplugin.h"
#include "pe_visualdescriptionofprofileplugin.h"
#include "pe_visualtargetplanerplugin.h"
#include "pe_visualaccessrightsplugin.h"
#include "pe_visualcreditandreferencesplugin.h"
#include "pe_commons.h"

PE_Commons::PE_Commons(QObject *parent)
    : QObject(parent)
{
    m_widgets.append(new PE_VisualStateLinePlugin(this));
    m_widgets.append(new PE_VisualToolBarPlugin(this));
    m_widgets.append(new PE_VisualDescriptionOfProfilePlugin(this));
    m_widgets.append(new PE_VisualTargetPlanerPlugin(this));
    m_widgets.append(new PE_VisualAccessRightsPlugin(this));
    m_widgets.append(new PE_VisualCreditAndReferencesPlugin(this));
    
}

QList<QDesignerCustomWidgetInterface*> PE_Commons::customWidgets() const
{
    return m_widgets;
}

Q_EXPORT_PLUGIN2(pe_commonsplugin, PE_Commons)
