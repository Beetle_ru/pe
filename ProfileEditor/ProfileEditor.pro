#-------------------------------------------------
#
# Project created by QtCreator 2012-04-25T11:35:43
#
#-------------------------------------------------

QT       += core gui

TARGET = ProfileEditor
TEMPLATE = app

DESTDIR = ../build/

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

LIBS += -L$$PWD/../build/ -lpe_commonsplugin

INCLUDEPATH += $$PWD/../PE_Commons
DEPENDPATH += $$PWD/../PE_Commons

TRANSLATIONS += ru.ts
TRANSLATIONS += en.ts
