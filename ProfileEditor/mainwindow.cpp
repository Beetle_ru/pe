#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <pe_formdescriptionofprofile.h>
#include <pe_commons.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    PE_Commons pe_c;

    (pe_c.customWidgets())[VDescriptionOfProfile]->createWidget(ui->tab_descr);
    (pe_c.customWidgets())[VStateLine]->createWidget(ui->tab_target);
    (pe_c.customWidgets())[VAccessRights]->createWidget(ui->tab_author);
    (pe_c.customWidgets())[VCreditAndReferences]->createWidget(ui->tab_credits);
}

MainWindow::~MainWindow()
{
    delete ui;
}
