#-------------------------------------------------
#
# Project created by QtCreator 2012-04-25T14:53:20
#
#-------------------------------------------------

QT       += core sql

QT       -= gui

TARGET = psql-conn-check
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

#LIBS += -L"C:\Program Files (x86)\PostgreSQL\8.2\bin"

#INCLUDEPATH += "C:/Qt/4.7.3/src/sql/drivers/psql"
SOURCES += main.cpp

LIBS += -L"/usr/local/pgsql/lib"
