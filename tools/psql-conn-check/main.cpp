//#define QT_PLUGIN
//#include <qsqldriverplugin.h>
//#include <qstringlist.h>
//#include "qsql_psql.cpp"
//#pragma message Q_SQL_EXPORT

#include <QtCore/QCoreApplication>
#include <QtSql/QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>
#include <stdio.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL", "pebsrv");
    db.setHostName("213.47.106.176");
    db.setPort(5432);
    db.setDatabaseName("peb");
    db.setUserName("peb_system");
    db.setPassword("#PeB@ClienT#");
    bool ok = db.open();
    QSqlQuery query(db);
    query.exec("SELECT uuid, name FROM indicator");
    query.exec("SELECT * FROM node");

    QSqlError er(query.lastError());
    printf("ok=%d query=<%d |%s>\n", ok, query.size(), er.text().toAscii().constData());
    query.first();
    printf("---------------------------\n");
    int i = 1;
    do {
        printf("%03d:contains |%s|%s|\n", i,
               query.value(0).toString().toAscii().constData(),
               query.value(1).toString().toAscii().constData()
        );
        i++;
    } while(query.next());
    printf("---------------------------");
    return a.exec();
}
