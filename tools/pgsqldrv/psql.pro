TARGET = qsqlpsql

SOURCES = main.cpp
include(qsql_psql/qsql_psql.pri)

include(qsqldriverbase/qsqldriverbase.pri)

LIBS += -L$$PWD"\pg8_commons_win32\lib"

INCLUDEPATH += $$PWD"\pg8_commons_win32\include"

DESTDIR = ../../build/
