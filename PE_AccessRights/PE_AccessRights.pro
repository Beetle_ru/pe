#-------------------------------------------------
#
# Project created by QtCreator 2012-04-25T11:22:11
#
#-------------------------------------------------

QT       += sql

QT       -= gui

TARGET = PE_AccessRights
TEMPLATE = lib
CONFIG += staticlib

DESTDIR = ../build/

SOURCES += pe_accessrights.cpp

HEADERS += pe_accessrights.h
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
