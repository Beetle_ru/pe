#-------------------------------------------------
#
# Project created by QtCreator 2012-04-25T11:11:50
#
#-------------------------------------------------

QT       += core gui

TARGET = PE_TestWidgets
TEMPLATE = app

DESTDIR = ../build/

SOURCES += main.cpp\
        mainwindow.cpp \
    dialoglanguage.cpp

HEADERS  += mainwindow.h \
    dialoglanguage.h

FORMS    += mainwindow.ui \
    dialoglanguage.ui


LIBS += -L$$PWD/../build/ -lpe_commonsplugin

INCLUDEPATH += $$PWD/../PE_Commons
DEPENDPATH += $$PWD/../PE_Commons


TRANSLATIONS += ru.ts
TRANSLATIONS += en.ts
