#ifndef DIALOGLANGUAGE_H
#define DIALOGLANGUAGE_H

#include <QDialog>
#include <QListWidgetItem>
#include <QTranslator>

namespace Ui {
class DialogLanguage;
}

class DialogLanguage : public QDialog
{
    Q_OBJECT
    
public:
    explicit DialogLanguage(QWidget *parent = 0, QTranslator * qt_translator = 0);
    ~DialogLanguage();
    
private slots:

    void on_listLanguage_itemClicked(QListWidgetItem *item);

private:
    Ui::DialogLanguage *ui;
    QTranslator * qt_trans;
    QWidget *mainWindow;
};

#endif // DIALOGLANGUAGE_H
