<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
<context>
    <name>DialogLanguage</name>
    <message>
        <location filename="dialoglanguage.ui" line="20"/>
        <source>Select</source>
        <translation>Select</translation>
    </message>
    <message>
        <location filename="dialoglanguage.ui" line="33"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialoglanguage.ui" line="38"/>
        <source>Russian</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Main Window</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="31"/>
        <source>Description of Profile</source>
        <translation>Description of Profile</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="36"/>
        <source>Target Planner</source>
        <translation>Target Planner</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="41"/>
        <source>Athorization</source>
        <translation>Athorization</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="46"/>
        <source>Credit and References</source>
        <translation>Credit and References</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="70"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
</context>
</TS>
