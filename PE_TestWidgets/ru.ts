<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>DialogLanguage</name>
    <message>
        <location filename="dialoglanguage.ui" line="20"/>
        <source>Select</source>
        <translation>Выбор</translation>
    </message>
    <message>
        <location filename="dialoglanguage.ui" line="33"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialoglanguage.ui" line="38"/>
        <source>Russian</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Главное окно</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="31"/>
        <source>Description of Profile</source>
        <translation>Описание</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="36"/>
        <source>Target Planner</source>
        <translation>Редактор целей</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="41"/>
        <source>Athorization</source>
        <translation>Авторизация</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="46"/>
        <source>Credit and References</source>
        <translation>Дополнительные параметры</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="70"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
</context>
</TS>
