#include "dialoglanguage.h"
#include "ui_dialoglanguage.h"
#include <QtDebug>

DialogLanguage::DialogLanguage(QWidget *parent, QTranslator * qt_translator) :
    QDialog(parent),
    ui(new Ui::DialogLanguage)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    qt_trans = qt_translator;
    mainWindow = parent;
}

DialogLanguage::~DialogLanguage()
{
    delete ui;
}




void DialogLanguage::on_listLanguage_itemClicked(QListWidgetItem *item)
{
    qDebug() << "Language Change " << item->text();
    qDebug() << "Language Russian " << (item->text() == QString("Russian"));
    qDebug() << "Language English " << (item->text() == QString("English"));

    QString file_translate;
    QTranslator *loadTester = new QTranslator;

    if(item->text() == QString("Russian")) {
        file_translate = QApplication::applicationDirPath () + "/locales/tw/ru.qm";
        mainWindow->setLocale(QLocale::Russian);
    }
    if(item->text() == QString("English")) {
        file_translate = QApplication::applicationDirPath () + "/locales/tw/en.qm";
        mainWindow->setLocale(QLocale::English);
    }
    qDebug() << "Locales file loaded " << loadTester->load(file_translate);

    if(loadTester->load(file_translate)) {
        qt_trans->load(file_translate);
    }
        else
            delete loadTester;


    this->close();
}
