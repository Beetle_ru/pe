#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTranslator>

#define VStateLine 0                  //PE_VisualStateLinePlugin
#define VToolBar 1                   //PE_VisualToolBarPlugin
#define VDescriptionOfProfile 2      //PE_VisualDescriptionOfProfilePlugin
#define VTargetPlaner 3               //PE_VisualTargetPlanerPlugin
#define VAccessRights 4               //PE_VisualAccessRightsPlugin
#define VCreditAndReferences 5        //PE_VisualCreditAndReferencesPlugin


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    void changeEvent(QEvent* event);
    ~MainWindow();
    
private slots:
    void on_buttonLanguage_clicked();

private:
    Ui::MainWindow *ui;
    QTranslator  *qt_translator;
};

#endif // MAINWINDOW_H
