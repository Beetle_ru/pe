#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <pe_formdescriptionofprofile.h>
#include <pe_commons.h>
#include "dialoglanguage.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qt_translator = new QTranslator;
    qApp->installTranslator( qt_translator );
    /*QString file_translate;
    file_translate = QApplication::applicationDirPath () + "/locales/pe/ru.qm";
    qt_trans->load(file_translate);*/
    PE_Commons pe_c;

    (pe_c.customWidgets())[VDescriptionOfProfile]->createWidget(ui->tab);
    (pe_c.customWidgets())[VAccessRights]->createWidget(ui->tab_3);
    (pe_c.customWidgets())[VCreditAndReferences]->createWidget(ui->tab_4);
    (pe_c.customWidgets())[VStateLine]->createWidget(ui->tab_2);
    (pe_c.customWidgets())[VToolBar]->createWidget(ui->widgetToolbar);



}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_buttonLanguage_clicked()
{
    DialogLanguage *dialog = new DialogLanguage(this,qt_translator);
    //DialogLanguage *dialog = new DialogLanguage(this);
    dialog->setGeometry(QRect(this->geometry().x() + ui->buttonLanguage->geometry().x(),
                              this->geometry().y() + ui->buttonLanguage->geometry().y(),
                              dialog->geometry().width(),
                              dialog->geometry().height()
                              ));
    dialog->show();
}

void MainWindow::changeEvent(QEvent* event)
{
    if (event->type() == QEvent::LanguageChange) {
        ui->retranslateUi(this);
        qDebug() << "Language Changed ";
        //this->setLocale(QLocale::Afan);
        qDebug() << "main language " << this->locale().language();
    }
}
